Example build: `docker build -t docker-inside-docker:v1 .`

Example run: `docker run -d --privileged  docker-inside-docker:v1`

![image.png](./image.png)
